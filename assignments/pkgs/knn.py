from numpy import random,argsort,sqrt,argmax,bincount,int_,array,vstack,round
from pylab import scatter,plot,show

def knn_search(x, D, K):
   """ find K nearest neighbours of data among D """
   ndata = D.shape[0]
   K = K if K < ndata else ndata
   # euclidean distances from the other points
   sqd = sqrt(((D - x)**2).sum(axis=1))
   idx = argsort(sqd) # sorting
   # return the indexes of K nearest neighbours
   return idx[:K]
 
def knn_classifier  (x, D, labels, K):
   """ Classify the vector x
       D - data matrix (each row is a pattern).
       labels - class of each pattern.
       K - number of neighbour to use.
       Returns the class label and the neighbors indexes.
   """
   neig_idx = knn_search(x,D,K)
   counts = bincount(labels[neig_idx]) # voting
   return argmax(counts)