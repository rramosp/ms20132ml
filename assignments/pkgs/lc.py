from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.svm import SVC
import numpy as np
import matplotlib.pyplot as plt
from pylab import *
from boot import *

# draw a learning curve of classifier performance with respect a dataset
# performing bootstrapping.
#
# dataset    - a dataset on the form (data, labels) as returned by sampleD1 or sampleD2
# classifier - s scikit classifier
# nTimes     - number of times bootstrapping is done
# stepSize   - the step size with which to sample test/train datasets.
#              a step size of 0.1 will sample a train dataset of 0.1, 0.2, 0.3,...0.9

def learning_curve(dataset, classifier, nTimes=20, stepSize=0.01):
    X,y = dataset
    pcts = np.arange(0.1, 1.0, stepSize)
    testScores = np.array([])
    trainScores = np.array([])
    for pct in pcts:
        print "test pct "+str(pct)
        s=bootstrap(dataset, classifier, 1-pct, nTimes)
        testScores  = np.append(testScores, s[0])
        trainScores = np.append(trainScores, s[1])
    
    ptrain = plt.plot(pcts, trainScores, c="b", label="train")
    ptest = plt.plot(pcts, testScores, c="r", label="test")
    ylim([0,1])
    legend(title=str(classifier)+" "+str(X.shape), loc=8)

