# --------------------------------------------------
function retval = kNNtrain(data,k)

     retval.data  = data;
     retval.k     = k;
     retval.predictFunction = @kNNpredict;

endfunction

# --------------------------------------------------
function retval = kNNpredict(m, x)

    data = m.data;
    k = m.k;
    [ndata, classcol] = size(data);
    classlist = unique(data(:,classcol));
    classdata = data(:,classcol);
    data(:,classcol)=[];

    nn= [];

    # replicate x to match datasize
    xr = repmat(x,ndata,1);
    distances = sum((data-xr).^2,2);
    dmax = max(distances);

    for i=1:k
       [nmax,idx] = min(distances);
       distances(idx)=dmax+1;
       nn = [nn classdata(idx)];
    endfor

    nnclasscount = [];
    for class=classlist'
        nnclasscount = [nnclasscount sum(nn==class)];
    endfor

    [nmax, idx] = max(nnclasscount);

    retval = classlist(idx);

endfunction
