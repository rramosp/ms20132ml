import numpy as np
from matplotlib import *
from matplotlib.pyplot import *
from scipy.interpolate import *

def mnorm(m1,m2,std1=1,std2=1):
   x1 = np.random.normal(m1,std1,1)[0]
   x2 = np.random.normal(m2,std2,1)[0]
   return np.array([x1,x2])

def sampleD1(nsamples):
   r=[]
   n=0
   labels=[]
   while n<nsamples:
     n=n+1
     if np.random.random()>0.5:
        x=mnorm(3,3)
        c=0
     else:
        x=mnorm(0,0)
        c=1
     r.append(x)
     labels.append(c)
   return (np.array(r),np.array(labels))

def sampleD2(nsamples):
   r=[]
   n=0
   labels=[]
   while n<nsamples:
     n=n+1
     if np.random.random()>0.5:
        x=mnorm(0,0)
        c=0
     else:
        x=mnorm(0,0)
        x=x*15*np.exp(-sum(x**2)**.5)
        c=1
     r.append(x)
     labels.append(c)
   return (np.array(r),np.array(labels))

def sampleD3(nsamples):
   r=[]
   n=0
   labels=[]
   while n<nsamples:
     n=n+1
     if np.random.random()>0.5:      
        x1=3*(np.random.random()-0.5)
        x2=x1**2+np.random.normal(0,1,1)
        x=np.array([x1,x2])
        c=0
     else:
        x1=3*(np.random.random()-0.5)
        x2=-x1**2+np.random.normal(0,1,1)+4
        x=np.array([x1-1.5,x2])
        c=1
     r.append(x)
     labels.append(c)
   return (np.array(r),np.array(labels))

def linear_classifier(x):
   xe = np.insert(x,0,1)
   w  = np.ones(xe.shape[0])
   w[0]=-1
   return 1 if sum(xe*w)<0.5 else 0

def vclassify(f,d,*args):
   """ uses a given function to classify an array of data
      f - classifier function must have as first arg the vector to classify
      d - the array of data to classify, row vectors will be
          fed one by one to function f
      args - extra args to be passed on to function f on each call
   """
   labels=[]
   for i in range(0,d.shape[0]):
      singlex=d[i]
      labels.append(f(singlex,*args))
   return np.array(labels)

def plot_boundary(f,pmin,pmax,*args):
   """ plots a 2D classification boundary
      f - classifier function (see vclassify above)
      pmin,pmax - plot x,y square box
      args - extra args for function f
   """
   # grid datapoints
   gx,gy=np.mgrid[pmin:pmax:200j,pmin:pmax:200j]
   gpoints = np.vstack([gx.flatten(),gy.flatten()]).T

   # classify grid datapoints
   p = vclassify(f, gpoints, *args)

   # plot classification boundary
   g = griddata(gpoints,p,(gx,gy),method="nearest")
   imshow(g.T, extent=(pmin,pmax,pmin,pmax), origin="lower")

   
