from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.svm import SVC
import numpy as np
import matplotlib.pyplot as plt
from pylab import *

# bootstrapping for a classifier and a dataset
#
# classifier - s scikit classifier
# dataset    - a dataset on the form (data, labels) as returned by sampleD1 or sampleD2
# testPct    - percentage of dataset used for training (between 0 an 1)
# nTimes     - number of times bootstrapping is done
#
# returns - an array with [mTest, mTrain, sTest, sTrain]
#           where mTrain is the mean of scores obtained in train data
#                 sTrain is the stdev of scores obtained in train data
#                 mTrain, sTrain are analogous but for train data
#
def bootstrap(dataset, classifier, testPct, nTimes):
    X,y = dataset
    testScores = np.array([])
    trainScores = np.array([])
    
    for i in range(nTimes):
        print "Bootstrapping nb "+str(i)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=testPct)
        classifier.fit(X_train, y_train)
        testScores  = np.append(testScores, classifier.score(X_test, y_test))
        trainScores = np.append(trainScores, classifier.score(X_train, y_train))
        
    return (np.array([np.mean(testScores), np.mean(trainScores), np.std(testScores), np.std(trainScores)]))
    
