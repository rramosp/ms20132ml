# --------------------------------------------------
# 2class 2D dataset. Classes are labelled 0 and 1
# --------------------------------------------------
function retval = sampleD1(nsamples)

    class0portion = 0.5;
    class1portion = 1-class0portion;
    class0data = [stdnormal_rnd(nsamples*class0portion,1)/2+0.5 stdnormal_rnd(nsamples*class0portion,1)/2+0.5 zeros(nsamples*class0portion,1)];
    class1data = [stdnormal_rnd(nsamples*class1portion,1)/2-0.5 stdnormal_rnd(nsamples*class1portion,1)/2-0.5 ones(nsamples*class1portion,1)];
    
    retval = [class0data; class1data];

endfunction

# --------------------------------------------------
# n class 2D dataset. Classes are labelled 0 and 1
# --------------------------------------------------
function retval = sampleD3(nsamples, nclasses)

    retval=[];
    for n=1:nclasses
        ns = int64(nsamples/nclasses);
        data = [stdnormal_rnd(ns,1)/2+n*0.5 stdnormal_rnd(ns,1)/2+n*0.5 n.*ones(ns,1)];
        retval = [retval; data];
    endfor
    
endfunction

# --------------------------------------------------
# 2class 2D dataset as a mixture of gaussians. Classes are labelled 0 and 1
# --------------------------------------------------
function retval = sampleD2(nsamples)

    class0portion = 0.5;
    class1portion = 1-class0portion;
    c1means = [0.5 0.5; 3.0 0.5];
    c2means = [1.5 -0.8];

    retval = [0 0 0];

    for i = 1:nsamples
        if (unifrnd(0,1,1,1)<class0portion)
           mean = c1means(floor(unifrnd(0,size(c1means)(:,1),1,1)+1), :);
           xmean = mean(1);
           ymean = mean(2);
           point = [stdnormal_rnd(1,1)+xmean stdnormal_rnd(1,1)/1.5+ymean 0];
        else
           mean = c2means(floor(unifrnd(0,size(c2means)(:,1),1,1)+1), :);
           xmean = mean(1);
           ymean = mean(2);
           point = [stdnormal_rnd(1,1)/2+xmean stdnormal_rnd(1,1)/2+ymean 1];
        endif
        retval(end+1, :) = point;
    endfor
    retval = retval(2:end,:);

endfunction

# --------------------------------------------------
# plots a 2D dataset, data are [x, y, class] vectors
# --------------------------------------------------
function plotDataset(data, model="NULL")
    
#    colors = [198 82 16; 37 98 213; 200 200 20; 159 201 90]./255;
    colors = [100 100 100; 150 150 150; 200 200 200; 159 201 90]./255;
    nbcolors = size(colors)(1);
    
    classcol = size(data)(:,2);
    classes = unique(data(:,classcol));
    color=1;
    minx = min(data(:,1));
    maxx = max(data(:,1));
    miny = min(data(:,2));
    maxy = max(data(:,2));
    if (!strcmp(model, "NULL"))
        bpoints=sampleBoundaryMap(model, [minx maxx], [miny maxy]);
    else
        bpoints=[0 0 classes(1)];
    endif
    for class = classes'
        class
        colorboundary = colors(color,:);
        colordata = colorboundary*0.7;
        colordata
    axis([minx maxx miny maxy]);
        plot(data(:,1)(data(:,classcol)==class), data(:,2)(data(:,classcol)==class), "+", "color", colordata, "markersize", 3, bpoints(:,1)(bpoints(:,3)==class), bpoints(:,2)(bpoints(:,3)==class), ".", "color", colorboundary);
        hold;
        color = color + 1;
        if (color > nbcolors)
            color = 1;
        endif
    endfor
endfunction


function retval = removeClass(data)
    
    retval = data;
    classcol = size(data)(:,2);
    retval(:,classcol)=[];
     
endfunction

function retval = getAccuracies(model,data)
    
    [ndata, classcol] = size(data)
    classes=data(:,classcol);
    classlabels = unique(classes);

    predictions = [];
    for d = data'
        d=d';
        d(end)=[];
        predictions(end+1,:) = model.predictFunction(model,d);
    endfor

    r=[];

    i=j=1;
    for class=classlabels'
        classidx = find(classes==class);
        classpredictions = predictions(classidx);
        for class2=classlabels'
            r(i,j) = sum(classpredictions==class2);
            j=j+1;
        endfor
        i=i+1;
        j=1;
    endfor
    retval = r;
endfunction


# --------------------------------------------------
# --------------------------------------------------
function retval=sampleBoundaryMap(model, xrange=[0, 1], yrange=[0, 1])
   retval = [];
   npoints = 10000;
   for i=1:npoints

      x = unifrnd(xrange(1), xrange(2), 1, 1);
      y = unifrnd(yrange(1), yrange(2), 1, 1);
      
      c = model.predictFunction(model, [x y]);
      retval(end+1, :) = [x y c];

   endfor
endfunction

function retval=simpleModel()
    retval.predictFunction = @simplePredict;
endfunction

function retval=simplePredict(m ,x)
    x = [x x(:,1)^2 x(:, 1)^2 x(:, 2)^4 x(:,2)^4];
    retval = ([1 x]*[0.5 3 2 0.5 1 0.5 0.01]'>0.5);
endfunction




