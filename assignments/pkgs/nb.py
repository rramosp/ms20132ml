import sys
import os
import numpy as np
sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

from defs import *

def gaus(x,m=0,s=1):
    f1 = 1/(s*(2*np.pi)**0.5)
    f2 = np.exp(-((x-m)**2)/(2*s**2))
    return ( f1*f2 )

def naiveBayesTrain(d,c):
    r=dict()
    labels = np.unique(c);
    r["labels"]=labels
    cmeans=[]
    cstdevs=[]
    for l in labels:
        dc = d[c==l];
        means = np.mean(dc,axis=0)
        stds  = np.std(dc,axis=0)
        cmeans.append(means)
        cstdevs.append(stds)
    r["cmeans"]  = cmeans
    r["cstdevs"] = cstdevs
    return r

def naiveBayesPredict(x,m):
    probs=[]
    for l in range(0,m["labels"].shape[0]):
        p=1
        means = m["cmeans"][l]
        stdevs = m["cstdevs"][l]
        for i in range(0,means.shape[0]):
            mean=means[i]
            stdev=stdevs[i]
            p=p*gaus(x[i],mean,stdev)
        probs.append(p)
    return m["labels"][np.argmax(probs)]
    
