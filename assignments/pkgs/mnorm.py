
import numpy

def mnormpdf(x,m,c):
  """ multivariate normal probability density function
      x: the point for which to return the probability
      m: the mean of the multivariate
      c: the covariance matrix

      dimensions of x,m,c must match
  """
  dimension = m.size
  detDiagCovMatrix = numpy.sqrt(numpy.prod(numpy.diag(c)))
  frac = (2*numpy.pi)**(-dimension/2.0) * (1/detDiagCovMatrix)
  fprime = x - m
  fprime **= 2
  #if log:
  #  logValue = -0.5*numpy.dot(fprime, 1/numpy.diag(c))
  #  logValue += numpy.log(frac)
  #  return logValue
  #else:
  return frac * numpy.exp(-0.5*numpy.dot(fprime, 1/numpy.diag(c)))
