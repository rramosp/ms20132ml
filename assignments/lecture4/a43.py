import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )
import numpy as np
import defs
from boot import *
from lc import *
from matplotlib.pyplot import *
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA

# FIRST: complete implementation in bootlc.py

d = defs.sampleD2(100)

# after doing this assignment you can try with toy datasets in scikit learn
#d = make_circles(noise=0.2, factor=0.5, random_state=1, n_samples=100)

# ---------- example 1 ------------
c = LDA() # a linear boundary
figure()
# call function learning_curve with stepsize of 0.1 and 10 bootstraps
# see source at ../pkgs/lc.py to see calling syntax
???

# ---------- example 2 -------------
c=SVC(gamma=2, C=1) # variable boundary
figure()
# call learning curve with stepsize of 0.1 and 10 bootstraps
???

# ---------- example 3 -------------
c=GaussianNB() # models the dataset distributions
figure()
# call learning curve with stepsize of 0.1 and 10 bootstraps
???

show()
    



