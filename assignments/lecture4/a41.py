import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )
import numpy as np
import defs
from boot import *
from lc import *
from matplotlib.pyplot import *
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA

(d,l) = defs.sampleD2(100)

# see http://scikit-learn.org/stable/modules/classes.html 
# for dpocumentation on classifier functions in Scikit Learn
# check also the source code of plot_classifier_comparison.py

# example 1
c = ??
c.fit(d,l)
print "score="+str(c.score(d,l))

# example 2
c = ???
c.fit(d,l)
print "score="+str(c.score(d,l))

# example 3
c = ???
c.fit(d,l)
print "score="+str(c.score(d,l))
    



