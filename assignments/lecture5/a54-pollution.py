import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )
import numpy as np
import defs
from boot import *
from lc import *
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
from lc import *
from matplotlib.pyplot import *

data = np.loadtxt("contaminacion-20k-9attrs.csv", delimiter=",", skiprows=1)
d=data[:,1:data.shape[1]+1]
c=data[:,0]

# example of adding x6*x7
d=np.hstack((d, d[:,[6]]*d[:,[7]]))

# add all squared columns and also columns with x6*x7,x5*x8 and x7*x8
d = ???

# normalize data
dmax=d.max(axis=0)
dmin=d.min(axis=0)
d=(d-dmin)/(dmax-dmin)

# create two classes by thresholding
c5 = (c>5)*1
dataset = (d,c5)

# create and train kNN 20, decision tree and gaussian classifier
# and plot learning curves

gnb= ???
figure(); learning_curve(dataset, gnb, nTimes=5, stepSize=0.1)

gnb= ???
figure(); learning_curve(dataset, gnb, nTimes=2, stepSize=0.1)

gnb= ???
figure(); learning_curve(dataset, gnb, nTimes=2, stepSize=0.1)

gnb= ???
figure(); learning_curve(dataset, gnb, nTimes=2, stepSize=0.1)

show()

