import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )
import numpy as np
import defs

from matplotlib.pyplot import *

data = np.loadtxt("mnist-small.csv", delimiter=",")

# extract columns 0 to 784 into a single data structure
d=data[:,0:784]

# extract column 785 (which is column nb 784 if starting at index 0)
c= ???

# for curiosity: see rows 208, 56 for an ambiguous digit image
# set i=208 instead of random

# extract one random row (image) 
i=np.int(np.random.rand(1)*d.shape[1])
print "showing row "+str(i)
xrandom = d[i]

# reshape row into 28x28 image (use nparray.reshape)
x =  ???

# display image
imshow(x, aspect="equal", interpolation="nearest", cmap = cm.Greys_r)
show()




