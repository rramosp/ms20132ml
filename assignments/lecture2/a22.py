import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

from defs import *
from matplotlib.pyplot import *
from nb import *

# create train and test dataset with 100 elements
n=1000
(trd,trc) = ??
(tsd,tsc) = ??

smax=tsd.max()
smin=tsd.min()

# train with naive bayes
m = ???

# plot boundary with trained model
???

# predict test classes from trained model
tsp = ???

# count number of correct predictions in test data
tsok = ??

print "test accuracy = "+str(tsok)+" / "+str(n)

# separate test classes
d0 = ??
d1 = ??

# plot class 0 in blue
??
# plot class 1 in red
??
show()
