import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

# sample and plot datasets
import defs
import nb
from numpy import *
from matplotlib import *
from matplotlib.pyplot import *

# sample training set
(traind, trainc) = defs.sampleD2(100)
# sample test set
(dr, dc) = defs.sampleD1(100)
smax=dr.max()
smin=dr.min()

m = nb.naiveBayesTrain(dr,dc)
defs.plot_boundary(nb.naiveBayesPredict,smin,smax,m)

# plot test dataset
dr0 = dr[dc==0]
dr1 = dr[dc==1]
scatter(dr0[:,0], dr0[:,1], c="b")
scatter(dr1[:,0], dr1[:,1], c="r")

show()
