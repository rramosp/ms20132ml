import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )
import numpy as np
import defs
from matplotlib.pyplot import *

def logRegressionTrain(X,c,alpha):
    # add a column of ones to x matrix
    X = np.hstack((np.array([np.ones(X.shape[0])]).T,X))

    # initialize weights with zeroes
    w = np.zeros(X.shape[1])

    # iterations
    for i in range(20):
        # update rule w = w + alpha*X'*(c-g(X*w))
        w = w + ... ???
    
    return w

def logRegressionPredict(x,w):
    # add 1 for bias
    xone = np.hstack(([np.ones(1)],[x]))[0]
    
    # threshold on g(w'x)>0.5
    return ... ???

def g(x):
    # compute g = 1 / (1-e^(-w'x))
    # warn! must work vectorized
   return ... ???