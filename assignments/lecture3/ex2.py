import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )
import numpy as np
import defs
import lr2
from matplotlib.pyplot import *

# complete implementation in lr.py

# sample train and test datasets
n=1000
(trd,trc) = defs.sampleD2(n)
(tsd,tsc) = defs.sampleD2(n)

# train log regression
m2 = lr2.logRegressionTrain(trd,trc,0.01)
print m2

# compute accuracy
predictions=defs.vclassify(lr2.logRegressionPredict,tsd, m2)
tsok = sum(tsc==predictions)
print "GD test accuracy = "+str(tsok)+"/"+str(n)

# plot boundary
(smax,smin) = (tsd.max(), tsd.min())
defs.plot_boundary(lr2.logRegressionPredict,smax,smin,m2)

# plot test dataset
(dr0,dr1) = (tsd[tsc==0], tsd[tsc==1])
scatter(dr0[:,0], dr0[:,1], c="b")
scatter(dr1[:,0], dr1[:,1], c="r")

show()
    



