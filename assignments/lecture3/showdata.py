import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )
import numpy as np
import defs
from matplotlib.pyplot import *

# FIRST: complete implementation in lr.py

# sample train and test datasets
n=100
(d1, c1) = defs.sampleD1(n)
(d2, c2) = defs.sampleD2(n)

# plot dataset1
f,ax = subplots(1,2)

(d10,d11) = (d1[c1==0], d1[c1==1])
ax[0].scatter(d10[:,0], d10[:,1], c="b")
ax[0].scatter(d11[:,0], d11[:,1], c="r")

(d20,d21) = (d2[c2==0], d2[c2==1])
ax[1].scatter(d20[:,0], d20[:,1], c="b")
ax[1].scatter(d21[:,0], d21[:,1], c="r")


show()
    



