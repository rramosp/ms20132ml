import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )
import numpy as np
import defs
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# FIRST: complete implementation in lr.py

# sample train and test datasets
n=1000
(d1, c1) = defs.sampleD2(n)

d2=np.hstack((d1,np.array([(d1**2).sum(axis=1)]).T))

# plot dataset1
fig = plt.figure()
ax1 = fig.add_subplot(121, projection='3d')
ax0 = fig.add_subplot(122)

(d10,d11) = (d2[c1==0], d2[c1==1])
ax0.scatter(d10[:,0], d10[:,1], c="b")
ax0.scatter(d11[:,0], d11[:,1], c="r")

ax1.scatter(d10[:,0], d10[:,1], d10[:,2], c="b")
ax1.scatter(d11[:,0], d11[:,1], d11[:,2], c="r")


plt.show()
    



