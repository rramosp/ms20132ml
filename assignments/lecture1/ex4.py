import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

# sample and plot datasets
import defs
import knn
from numpy import *
from matplotlib import *
from matplotlib.pyplot import *
from scipy.interpolate import *

defs.plot_boundary(defs.linear_classifier,-10,10)

# sample and classify test dataset
(dr, dc) = defs.sampleD1(100)
dp = defs.vclassify(defs.linear_classifier, dr)

# calc test accuracy
testacc  = float(sum(dp==dc))/dc.shape[0]
print "testacc="+str(testacc)

# plot test dataset
dr0 = dr[dc==0]
dr1 = dr[dc==1]
scatter(dr0[:,0], dr0[:,1], c="b")
scatter(dr1[:,0], dr1[:,1], c="r")

# mark test points incorrectly classified
drbad = dr[nonzero(dc!=dp)];
plot(drbad[:,0], drbad[:,1],'o',markerfacecolor='None',markersize=10,markeredgewidth=1)
show()
