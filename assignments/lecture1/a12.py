import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

from defs import *
from matplotlib.pyplot import *
from knn import *

# create train and test dataset with 100 elements
n=100
(trd,trc) = ??
(tsd,tsc) = ?? 

k=15
smax=tsd.max()
smin=tsd.min()

plot_boundary(knn_classifier,smin,smax,trd,trc,k)

# predict test classes with knn from train data
tsp = vclassify(knn_classifier, tsd, trd, trc, k)

# Q: what is the structure of variable 'tsp'?

# count number of correct predictions in test data
tsok = ??

print "test accuracy = "+str(tsok)+" / "+str(n)

# now separte classes in test set and plot them like in A1.1

show()
