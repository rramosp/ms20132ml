import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

# sample and plot datasets
import defs
import knn
from numpy import *
from matplotlib import *
from matplotlib.pyplot import *
from scipy.interpolate import *
from sklearn.datasets import make_moons, make_circles, make_classification

# sample training set
(traind, trainc) = defs.sampleD2(100)
#(traind, trainc) = make_moons(noise=0.3, random_state=0)
(dr, dc) = defs.sampleD2(100)
#(dr, dc) = make_moons(n_samples=1000, noise=0.3, random_state=0)

smax=dr.max()
smin=dr.min()

k=20

defs.plot_boundary(knn.knn_classifier,smin,smax,traind,trainc,k)

# classify test dataset
dp = defs.vclassify(knn.knn_classifier, dr, traind, trainc, k)

# calc test accuracy
testacc  = float(sum(dp==dc))/dc.shape[0]
print "testacc="+str(testacc)

# plot test dataset
dr0 = dr[dc==0]
dr1 = dr[dc==1]
scatter(dr0[:,0], dr0[:,1], c="b")
scatter(dr1[:,0], dr1[:,1], c="r")

# mark test points incorrectly classified
drbad = dr[nonzero(dc!=dp)];
plot(drbad[:,0], drbad[:,1],'o',markerfacecolor='None',markersize=10,markeredgewidth=1)
show()
