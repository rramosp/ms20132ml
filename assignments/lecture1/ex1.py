import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

# sample and plot datasets
import defs
from matplotlib import *
from matplotlib.pyplot import *
from scipy.interpolate import *
from numpy import *

nplots = 2;
f,ax  = subplots(1,nplots)
for i in range(0,nplots):
   (d,c) = defs.sampleD1(100)
   d0 = d[c==0]
   d1 = d[c==1]
   dmax=d.max()
   dmin=d.min()
   gx,gy=np.mgrid[dmin:dmax:30j,dmin:dmax:30j]
   g = griddata(d,c,(gx,gy),method="nearest")
   ax[i].imshow(g.T, extent=(dmin,dmax,dmin,dmax), origin="lower")
   ax[i].scatter(d0[:,0], d0[:,1], c="b")
   ax[i].scatter(d1[:,0], d1[:,1], c="r")

show()


