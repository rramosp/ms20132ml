import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

from defs import *
from matplotlib.pyplot import *

# create dataset with 100 elements
# do this assignment twice once with sampleD1 and then with sampleD2
(d,c) = ???

# separate class 0
d0 = d[c==0]

# Q: why did the previous statement work?

# separate class 1
d1 = ???

# plot class 0
scatter(d0[:,0],d0[:,1],c="b")

# plot class 1 (use color "r" for red)
???


show()
