import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

import mnorm
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import mpl_toolkits.mplot3d.axes3d as axes3d

m=np.array([1,1])
c=np.array([[1,0.2],[1,0.5]])

Ys=np.arange(-2,5,0.07)
Xs=np.arange(-2,5,0.07)
d=np.meshgrid(Xs,Ys)
dd=np.array(d)
z=mnorm.mnormpdf(dd.T,m,c)
fig = plt.figure()
ax1 = fig.add_subplot(121, projection='3d')
ax2 = fig.add_subplot(122)
ax1.plot_surface(d[0],d[1],z,rstride=4, cstride=4, alpha=0.4,cmap=cm.jet)
ax2.contourf(d[0],d[1],z, zdir='z', cmap=cm.coolwarm)

plt.show()
