import sys; import os; sys.path.append( os.path.join( os.getcwd(), '../pkgs' ) )

# sample and plot datasets
import defs
import knn
from numpy import *

# sample train and test data
(traind,trainc) = defs.sampleD1(100)
(testd,testc)   = defs.sampleD1(500)

# classify train data
trainp = defs.vclassify(knn.knn_classifier, traind, traind, trainc, 10)

# classify test data
testp = defs.vclassify(knn.knn_classifier, testd, traind, trainc, 10)

# compute classification accuracies
trainacc = float(sum(trainp==trainc))/trainc.shape[0]
testacc  = float(sum(testp==testc))/testc.shape[0]

print "trainacc="+str(trainacc)
print "testacc="+str(testacc)
